#--------- Notify-Hipchat-v2 / using API v2
<#
.PARAMETER apitoken
    Required. Your HipChat API token, that you can create here https://www.hipchat.com/admin/api
.PARAMETER roomid
    Required. The id of the HipChat room you want to send the message to, find the id from here https://infigosoftware.hipchat.com/history
.PARAMETER message
    Required. The message body. 10,000 characters max.
.PARAMETER colour
    The background colour of the HipChat messag. One of "yellow", "red", "green", "purple", "gray", or "random". (default: yellow)
.PARAMETER notify
    Set whether or not this message should trigger a notification for people in the room (change the tab color, play a sound, etc). Each recipient's notification preferences are taken into account. 0 = false, 1 = true. (default: 0)
.PARAMETER apihost
    The URI of the HipChat api (default: api.hipchat.com)
.PARAMETER messsage_format
    Optional. Message type text or html.
.PARAMETER proxy
    Proxy url if any.
#>

Param(
    [Parameter(Mandatory = $True)]
    [string]$apitoken,       
    [Parameter(Mandatory = $True)]
    [string]$roomid,
    # Default: Has "(successful)" emoticon
    [Parameter(Mandatory = $False)] 
    [string]$messageText = "(successful)",
    [Parameter(Mandatory = $False)]
    [string]$apihost,
    [Parameter(Mandatory = $False)]
    [string]$color = 'green',
    [Parameter(Mandatory = $False)]
    [string]$proxy,
     [Parameter(Mandatory = $False)]
    [string]$message_format = 'text'
)

if ($apihost -eq "") {
    $apihost = "api.hipchat.com"
}   

if ($color -eq "") {
    $color = "yellow"
}

if ($message_format -eq "") {
    $color = "text"
}


try {


    $message = New-Object PSObject 

    $message | Add-Member -MemberType NoteProperty -Name color -Value $color
    $message | Add-Member -MemberType NoteProperty -Name message -Value "$messageText"
    $message | Add-Member -MemberType NoteProperty -Name notify -Value $False
    $message | Add-Member -MemberType NoteProperty -Name message_format -Value "text"
          

    #
    # Do the HTTP POST to HipChat
    # See: https://www.hipchat.com/docs/apiv2/method/send_room_notification
    # 
    $uri = "https://$apihost/v2/room/$roomid/notification?auth_token=$apitoken"
    $postBody = ConvertTo-Json -InputObject $message
    $postStr = [System.Text.Encoding]::UTF8.GetBytes($postBody)

    $webRequest = [System.Net.WebRequest]::Create($uri)
    $webRequest.ContentType = "application/json"
    $webrequest.ContentLength = $postStr.Length
    $webRequest.Method = "POST"

    # Set proxy if any
    if ($proxy -ne "") {
        $proxyObject = new-object System.Net.WebProxy $proxy
        $webRequest.proxy = $proxyObject
    }

    $requestStream = $webRequest.GetRequestStream()
    $requestStream.Write($postStr, 0,$postStr.length)
    $requestStream.Close()

    $webResponse = $webRequest.GetResponse()

    # Confirm Status Code : should be 204 / NoCintent
    Write-Host ("Status {0}:`t{1}" -f [int]$webResponse.StatusCode, $webResponse.StatusCode)
    $sr = New-Object IO.StreamReader($webResponse.GetResponseStream(), $webResponse.ContentEncoding)
    $content = $sr.ReadToEnd()
    $sr.Close()
    $webResponse.Close()

} catch [System.Net.WebException]{

    Write-Host "$apihost is not a valid host name, please be sure to use the correct host name. `r`n $_.Exception.ToString()" 

} catch [Exception] {
    Write-Host "Woh!, wasn't expecting to get this exception. `r`n $_.Exception.ToString()"
}
