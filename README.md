#HipChatPs

This repository stores small PowerShell Scripts to post HipChat via API, especially version2.


## Requirements

- Install PowerShell 3.0
 - Default windows 7 or Server are installed PowerShell 2.0, but in my script, ConvertTo-Json method is used to handle json object.
 - PowerShell 3.0 is incuded in .NET Framework 4, so please follow .NET Framework 4 installation guide. (Maybe you have to restart Windows OS after installing .NET Framework 4.)

## Getting Started

- First, allow the execution of PowerShell script on your window machine.
- Ref: http://technet.microsoft.com/en-us/library/hh849812.aspx 

```
Set-ExecutionPolicy RemoteSigned
```

- Copy this module to any location found in $env:PSModulePath
- Import the module

```
C:\PS>Import-Module Import-Module .\Notify-HopChat.psm1
```
	
1. List available command	

```
C:\PS> Get-Module Notify-HipChat
```
	
1. Get help on the module

To post message:

```
C:\PS>Get-Help Notify-HipChat -examples	
C:\PS>Get-Help Notify-HipChat -detailed
```

To get message:

```
C:\PS>Get-Help Read-HipChat -examples	
```
	
1. Execute the module

```
C:\PS>Notify-HipChat -apitoken YOUR_API_TOKEN -roomid NNN -messageText "(successful) Sample text."		
C:\PS>Read-HipChat -roomid xxx -apitoken YOUR_API_TOKEN -max_results 5
```

- Execute powrshell script like this:

```
PS C:\Users\xxx>Notify-HipChat -color 'green' -messageText "(successful) Sample text." 
 -roomid xxx -apitoken YOUR_API_TOKEN
```

- You can use not only hipchat.com but also on-premises HipChat server.


## References

- https://github.com/lholman/hipchat-ps
 - Powershell script for sending a HipChat message using their REST API (for v1)


## TODO

- Enabled to get HipChat message.
- Made scriptmodule and so on...