#HipChatPs

このリポジトリには、HipChatのAPIを、Power Shellで操作するための簡単なスクリプトを登録しています。
つたないスクリプトですが、何かの参考になれば幸いです。

## 前提条件

- Install PowerShell 3.0
 - Power Shell3.0に依存する関数を一部利用していますので、.NET Framework 4を組み込んでバージョンを上げてください。
　- 標準のwindowsだと、Powershell2.0になります。 

## まずはじめに

- Powershellスクリプトが実行出来るように、ポリシーを調整してください。
- Ref: http://technet.microsoft.com/en-us/library/hh849812.aspx 

```
Set-ExecutionPolicy RemoteSigned
```

- このモジュールを $env:PSModulePath　のパスの通ったところに置いてください。
- モジュールのインポートをします。

```
C:\PS>Import-Module Import-Module .\Notify-HopChat.psm1
```
	
1. このモジュールで利用できるコマンドを表示します	

```
C:\PS> Get-Module Notify-HipChat
```
	
1. ヘルプは下記のとおりです

		C:\PS>Get-Help Notify-HipChat -examples
	
		C:\PS>Get-Help Notify-HipChat -detailed

- スクリプトが実行できるようになたら、下記のように引数を指定して、Hipchatの指定のroomに投稿ができます:

```
PS C:\Users\xxx> .\Notify-HopChat -color 'green' -messageText "(successful) Sample text." 
 -roomid xxx -apitoken YOUR_API_TOKEN
```

- HipChat.comだけでなく、オンプレ版のHipchatサーバに対しても、投稿可能です。
-　引数でproxyの指定も可能です。（ただいま認証なしプロキシのみ）

## 参考

- https://github.com/lholman/hipchat-ps
 - Powershell script for sending a HipChat message using their REST API (for v1)


## TODO

- 投稿だけでなく、メッセージの取得もできるようにする。
- 関数、モジュール化する...